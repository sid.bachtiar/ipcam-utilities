<?php

$name=$argv[1]; // e.g.: 'ipcam1'
$url=$argv[2]; // e.g.: http://user:pass@address/auto.jpg?
$baseDir=$argv[3]; // e.g.: /home/user/Downloads/ipcam/

$min = date('i');
while ($min == date('i')) {
    download($name, $url, $baseDir);
}

function download($name, $url, $baseDir) {
    $dir = date('Y/m/d/H/H_');

    $i = (int) date('i');
    if ($i > 1) {
        $minute = ((int) (($i - 1) / 15)) * 15;
    } else {
        $minute = 0;
    }
    $dir .= str_pad($minute, 2, '0', STR_PAD_LEFT);

    $dirPath = $baseDir.$dir;
    mkdir($dirPath, 0777, true);
    $fileName = date('Y_m_d_H_i_s_').gettimeofday()['usec'].'.jpg';

    $url .= '&'.date('YmdHis').rand(100,999);

    $command = "cd $dirPath; wget --output-document $fileName '$url'";
    echo $command."\n";
    exec($command);
}

