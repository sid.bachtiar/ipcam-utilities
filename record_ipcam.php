<?php

$name=$argv[1]; // e.g.: 'ipcam1'
$url=$argv[2]; // e.g.: http://address:99/videostream.cgi?user=username&pwd=pass
$baseDir=$argv[3]; // e.g.: /home/user/Downloads/ipcam/
$action=$argv[4]; // e.g.: 'split' to stop any running recording (so the video will be finalised and can be viewed) and start another one

$baseFile=$name.'_'.date('Y_m_d');
$currentProcess=[];

if ($action === 'split')
{
    killAnyProcess($name);
    sleep(1);
    startRecording($baseDir, $baseFile, $url);
}
else
{
    $command = "ps ax | grep $baseFile | grep -v grep";
    echo $command."\n";
    exec($command, $currentProcess);
    print_r($currentProcess); echo "\n";

    // if there is no current process detected for today
    if (!$currentProcess || empty($currentProcess) || !count($currentProcess))
    {
        killAnyProcess($name);
    }
    else
    {
        // check if the recording is still going
        // by the size of the file
        $tmp = explode("/", $currentProcess[0]);
        $currentFile = array_pop($tmp);
        echo "Current file = ".$currentFile."\n";
        $fileSizeSample1 = filesize($baseDir.$currentFile);
        echo "file size sample 1 = $fileSizeSample1\n";
        sleep(5);
        clearstatcache();
        $fileSizeSample2 = filesize($baseDir.$currentFile);
        echo "file size sample 2 = $fileSizeSample2\n";
        if ($fileSizeSample1 == $fileSizeSample2)
        {
            echo "File size remains the same, recording has stopped\n";

            $pid = preg_split("/\\s/", $currentProcess[0])[0];
            $command = "kill -SIGINT $pid";
            echo $command."\n";
            exec($command);
        }
        else
        {
            echo "File size is changing, recording is still going\n";
        }
    }

// recheck if there's any today's recording going
    $command = "ps ax | grep $baseFile | grep -v grep";
    echo $command."\n";
    $currentProcess=[];
    exec($command, $currentProcess);
    print_r($currentProcess); echo "\n";

// if there is no current process detected for today
// start one
    if (!$currentProcess || empty($currentProcess) || !count($currentProcess))
    {
        startRecording($baseDir, $baseFile, $url);
    }
}

function startRecording($baseDir, $baseFile, $url)
{
    $filePath = $baseDir.$baseFile.'_'.date('H_i_s').'.mp4';
    //$command = "cvlc \"$url\" --sout file/mp4:$filePath > /dev/null 2>/dev/null &";
    $command = "cvlc \"$url\" --sout \"#transcode{vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100,sfilter=marq{marquee='%Y-%m-%d %H:%M:%S',position=4,color=0xFF0000,size=16}}:file{dst=$filePath}\" > /dev/null 2>/dev/null &";
    echo $command."\n";
    $output = [];
    exec($command, $output);
    print_r($output); echo "\n";
}

function killAnyProcess($name)
{
    // see if there is any other process related to this ipcam, e.g.: yesterday's recording
    // that needs to be killed
    $anyProcess=[];
    $command = "ps ax | grep $name | grep -v grep | grep -v ".basename(__FILE__);
    echo $command."\n";
    exec($command, $anyProcess);
    print_r($anyProcess); echo "\n";
    // if there's any, kill it
    if (count($anyProcess))
    {
        foreach ($anyProcess as $process)
        {
            $pid = preg_split("/\\s/", $process)[0];
            $command = "kill -SIGINT $pid";
            echo $command."\n";
            exec($command);
        }
    }
}
